# eWebResults Vagrant Box #

### Current Version: 1.0.2 ###
(see upgrading information below)

Also check out [Wiki](https://bitbucket.org/e-webresults/vagrant/wiki/Home) for more information.

#### What is this? ####
To work with us, work fast and efficiently you will need a good set of tools, a good development environment. This is it essentially, and you can get it all within a few minutes (depending on how fast your computer is).

#### How to get help ####
email: [jaygaura@ewebresults.com](mailto:jaygaura@ewebresults.com)
Skype: jay.gaura
Vagrant Share (see below)

#### Preparation ####

You will need a computer with a Windows, Mac or Linux OS.
You will also need the following software pieces installed (all free and cross-platform):
* [VirtualBox](https://www.virtualbox.org/)
* [Vagrant](https://www.vagrantup.com/)
* and if you are on Windows you will also need [Git Bash](https://git-scm.com/). Please read and follow [these instructions](http://blog.osteel.me/posts/2015/01/25/how-to-use-vagrant-on-windows.html). If you prefer you can use [Sygwin](https://www.cygwin.com/) instead. This is basically a substitute for Linux or Mac Terminal. For simplicity we will refer to either of the tools as Terminal later in this document unless a specific tool must be named in the context.
If you opt for using Cygwin when installing Cygwin on Windows you should [enable openssh](https://youtu.be/CwYSvvGaiWU), you will need it later.
After you install either Git Bash or Cygwin on Windows you may need to add  a line "export VAGRANT_DETECTED_OS=cygwin" to ~/.bashrc file. If you don't know how to do that simply open your terminal window (run the program) and paste or type this in `echo "export VAGRANT_DETECTED_OS=cygwin" >> ~/.bashrc` and hit ENTER. Then you should exit out of ssh (`exit`) and restart your terminal. When you fire your terminal up again you will probably see a couple of warnings saying that since you added the .bashrc file a couple of other files had to be added. That's fine.
If you will use [Git Bash](https://git-scm.com/) (recommended) you will need to run the program as Administrator. You can configure the shortcut to start the program in Administrator mode each time you click on it. [Here is how to do that](http://www.cnet.com/how-to/always-run-a-program-in-administrator-mode-in-windows-10/).

Finally to use vagrant share feature you will need to [open a free account with Atlas Hashicorp (company behind vagrant)](https://atlas.hashicorp.com/session).

#### SetUp [VIDEO](https://youtu.be/K9MncaEu8YM) ####

1. You should place the contents of the vagrant zip file you have gotten from us (the one that contains this README.md) in a place on your hard drive. Your projects will be inside the `/projects` folder.
2. Copy `vagrant/config-sample` to `vagrant/config` and using your favorite code editor edit it - fill in all the missing fields. The file is well commented so this should be fairly easy. All the fields are required. Do not move to the next steps until all the fields in your config file are properly filled. The information you put in there will stay private. We will only pull your name and email address, which we will need to know anyway if we end up working together. The last two fields `server_email` and `server_email_pass` are also very important. Your gmail account will be used to send out emails from the server - to check form submissions and stuff like that. Make sure those all correctly filled out. You can always create a new gmail account for that. Make sure that [this setting](https://www.google.com/settings/security/lesssecureapps) is on. We had reports that emails weren't working without this.
3. Open `Vagrantfile` in your favorite code editor and look for line `vb.memory = 1024` (could be #16). If you have more than 4Gb of RAM installed on your HOST machine you may want to up this to 2048 or more. Your server will install and run faster. However this is not required. Also if you set the RAM for your virtual box to 4096 or more you might want to change the server OS to 64 bits - change line ~#6 `config.vm.box = "ubuntu/trusty32"` to `config.vm.box = "ubuntu/trusty64"`. You might also want to change the IP address (line ~#9) `config.vm.network "private_network", ip: "10.0.0.10"`, however this is not required, and if you do need to change it only change the last bit marked by ** in `10.0.0.**`. For example `10.0.0.11` or `10.0.0.12` will work. You would only want to do that in case port IP 10.0.0.10 is already used (for another vagrant box for example).
4. Fire up your Terminal and cd into the directory where you have all your vagrant files. I.e. do something like `cd ~/eweb`. If you do `ls -la` you should see all the directories in there such as "vagrant", "bash" and "projects". Then you know you are in the right place.
5. Now just run "vagrant up"
6. If you set your config file properly this should run without interruption or needing user (your) input. So you can pretty much take a few minutes-walk walk or something. But the time you come back you should have it all setup. You will know that all is done if the last line reads "==> default: --- Finished provisioning ---". Basically what just happened is your virtual server is set up, and now you can use it.

#### Booting up your vagrant box, and shutting it down ####

The traditional vagrant commands  for that are `vagrant up --no-provision` (booting up) and `vagrant halt` (shutting down). You can set aliases (shortcuts) for ease of use.
Here is how you do it.
1. if you are on mac or linux or if you are familiar with inline editors like vi or nano you can simply open and edit the file like so `vi ~/.bashrc` or `nano ~/.bashrc`.
2. if you are not familiar you can do this `cd ~ && pwd` - this will print the location of your .bashrc file. Note the dot in front of it - it's a hidden file. So when you browse there using your file manager make sure it's set to show hidden files. Locate .bashrc in there, open it in an editor of your choice and then paste in the following 2 lines at the bottom of it. You would need to do this also if you were to use the vi or nano.

`alias vhalt='vagrant halt'
alias vup='vagrant up --no-provision'
`
Note: you copuld also add your own shortcuts in there for example you could add a shortcut (alias) for easy jumping into directory of your vagrant box, like so
`alias www='cd /path/to/your/project'`
you can replace `www` with anything that makes sense to you, and sure you will need to replace the path.

This way when you want to boot your vagrant and start working all you will need to do is open up your gitbash or whatever command line program you are using and then type `www` (in case above), followed by `vup` and then `vagrant ssh` (see below).

#### How to SSH into your Virtual Server ####

In case you don't know, ssh-ing into another computer is a way to connect and then control that computer remotely - run commands in it.
1. Open up your Terminal.
2. cd into the root directory of your vagrant setup, i.e. `cd ~/eweb`
3. Run "vagrant ssh" to ssh into your vagrant virtual server
4. Run "exit" to get out of it back to your Home or host machine.

#### Deployment Key setup ####

Deployment key is basically your new virtual server's public ssh key that we will add to our bitbucket repositories so that you can then properly authenticate yourself and pull the project files from there. Most everything is done automatically so don't worry if this does not make perfect sense yet.
These steps only need to be followed once with a new server.

1. SSH into your vagrant box. i.e. `cd ~/eweb` and `vagrant ssh`.
2. Run `dkey` - this command will run automated script that will pretty much do everything for you. When you see "Ready?" hit ENTER and then hit ENTER 3 more times responding to prompt questions. Then we should have received your key.
3. Get in touch with Jay Gaura (skype jay.gaura) and confirm that your deployment key is added and that you can continue with the site setup. DO NOT follow the next steps before you do this. It WON'T work before your deployment key is added. Also most likely you will receive username from us at this step, which you will also need.

#### Site SetUp ####

IMPORTANT: First make sure that you get a confirmation from us that your deployment key is added and that you have your user from us.

There are 2 types of site scripts: "new" and "old". If you are to create a new site for us, you will be running the `new` script, if you will be making some modifications for an existing site, you will want to run `old` script, and if you will be converting an existing site to wp-base theme you will need to run `convert`.
What this different commands do in essence they pull all the files and then set the site(s) completely for you. Basically after the script ran (usually takes several minutes) you should be able to simply visit your site in the browser, open it in your IDE and start developing.

1. SSH into your vagrant box. i.e. `cd ~/eweb` and `vagrant ssh`.
2. Run your script: type either `new` or `old` or else `convert` depending on what you need and hit ENTER.
3. If you vagrant box is new (you have not pull anything from our server) you will need to accept the connection: type "yes" and hit ENTER when you see something like "Are you sure you want to continue connecting (yes/no)?".
4. Then you will need to input user you received from us and desired url without the "http://" something like "mysite.new" for a new site and "mysite.old" for an old site of the same - if you are converting - so you see and compare them, import the content from the old, etc. It is also recommended that use production site URL and just replace the ".com" with ".new" or ".old" or any other local extension you will prefer.
5. Come back in a few minutes and your site will be setup - you will see URLs for front-end and back-end printed on the screen.
6. The user name is always "eweb-admin" and the password is whatever you set in the config file.
7. As the message will remind you you will need to make records for the URL you use in your host machine (main machine) hosts file. On Linux and Mac that's in `/etc/hosts` and on Windows that's in `C:\Windows\System32\Drivers/etc/hosts`. The line should be `10.0.0.10 mysite.new` or/and `10.0.0.10 mysite.old`. If you changed the IP address in your Vagrantfile than use the one you have there.
8. Your working project files will be at projects/{USER} under your vagrant root. You can set your IDE project there. We highly recommend that you use Netbeans with 2 spaces for tabs. This way git won't be confused as much about the semantics of our code when we collaborate on the same site.

#### Accessing Localhost and PhpMyAdmin ####

So the sites on your vagrant box will have their own urls, which you will also need to add to your host machine's hosts file as described above.
In addition you should also add a generic for the vagrant box. Something like [http://eweb/](http://eweb/) To do that simply add `10.0.0.10 eweb` to the hosts file on your host machine. Then you will be able to access PhpMyAdmin at [http://eweb:81](http://eweb:81).

#### Vagrant Commands ####

* `vagrant up` - setup vagrant virtual server or boot it up after it was halt or suspended (see below).
* `vagrant destroy` - wipes out your vagrant setup - don't use unless you have to, especially after you already set up deployment keys. We don't want to have to be adding them too often. If you are on Windows you might have to do `vagrant destroy -f`. the -f directive enforces the command. Otherwise you might get a warning about TTY incampatibility.
* `vagrant halt` - is like temporarily shutting down your server. Good to use at the end of the day.
* `vagrant suspend` - is like suspending your server - memory contents will be written down to Hard Disk, rebooting is much quicker than after halt.
* `vagrant ssh` - ssh into the vagrant box. Use `exit` to get out of there.
* `vagrant status` - check vagrant status
* `vagrant login` - log into atlas (needed to use the share feature)
* `vagrant share` - initialize the share - URL will be printed on the screen - give us this so we can check the progress or to help with something.
* `vagrant share --ssh` - same as above but ssh rather than http - Name will be printed on the screen - give us that...
* check [vagrant site](https://www.vagrantup.com/) for more..

#### Aliases - shortcodes you can use inside the vagrant box ####

* `www` - jump into `/var/www`.
* `wpb` - jump into WP-Base if installed (`/var/www/wpbase/public_html`)
* `ares` - restart apache (`sudo service apache2 restart`).
* `h` - jump home (`cd ~`).
* `..` - directory up the tree (`cd ..`).
* `-` - previous directory (`cd -`).
* `hosts` open the hosts file for editing (using nano).
* `hosts2` open the /etc/apache2/sites-available/default file for editing (using nano).
* `upd` - check Ubuntu updates.
* `upg` - run Ubuntu upgrade.
* `wpdb` - takes a dump of your database and puts it in your installation root as local.sql - needs to be run from the root of the site
* `perm` - set the right wordpress files permissions and ownership for all the files and folders inside wp-content
* `permall` - set the right wordpress files permissions and ownership for all the files and folders inside in wordpress site (including wp-admin and wp-includes) - needs to be run from the root of the site
* `wpcli` - update WP-CLI (you should run this once in a while)
* `perm` - set the right wordpress files permissions and ownership for all the files and folders inside wp-content - needs to be run from the root of the site
* `updatealiases` - can be run when vagrant box update is done manually (after `bash` and `vagrant` folders are replaced)
* `dbs` - sync databases - imports a database and replaces URLs for the site to work on localhost - needs to be run from the root of the site
* `new` - create a startup site when building a new site
* `old` - creating a local version of an existing site (usually with the purpose of making some updates to it)
* `convert` - creating a startup site and a local copy of an existing site that will be converted to WP-Base framework
* `wpbupd` - update wp-base for a local site - needs to be run from the root of the site
* `updatevagrantbox` - update vagrant box (replaces the `bash` and `vagrant` folders with the new ones and renews bash aliases
* `pull` - pulls updates from bitbucket repo (alias for `git pull bb master`)
* `push` - pushes updates to bitbucket repo (alias for `git push bb master`)
* see more in ~/.bash_aliases
* if you want to add your own custom aliases you sure can do this. Please add them in ~.bash_user_aliases file like so `echo "alias myalias='cd /var/www/mysite/public_html'" >> ~/.bash_user_aliases`. This will ensure that no aliases are lost during Vagrant Box software upgrade.

#### Gulp commands (inside site's root) ####

* `gulp` - concatenates all the css (less) and js files.
* `gulp watch` - watches for changes in less files and runs gulp every time changes are saved.
* `gulp watch-js` - the same as above but for js files.
* `gulp minify` - minifies all the css and js files.

#### Upgrading Vagrant Box [VIDEO](https://youtu.be/GtPUPBkwtag) ####

Note: as of version 1.0.1 this all can be done by script - just type `updatevagrantbox`. If this returns an error you might need to update manually one last time as decribed below and in the video above.

Lots of new features are added to the software weekly and sometimes daily.
To upgrade your Vagrant box follow the following steps:
* Halt the box if had it up running `vagrant halt`
* Download the new zip files from the bitbucket and unzip it somewhere on your Hard Drive.
* replace 2 folders: `vagrant` and `bash` from your current vagrant box with the ones from the newly downloaded package.
* Do `vagrant up`
* SSH into your Vagrant box `vagrant ssh`
* Run `upgradealiases` command. If your Vagrant box is quite out of date and the command won't work you will need to run `/var/bash/updatealiases.sh` instead.
* Run `exit` to exit out of SSH
* Run `vagrant ssh` to ssh back in. You can use the new aliases now and your vagrant box is updated.
* Optionally you can also upgrade your Ubuntu server now: `upd` and `upg`.

#### Installing WP-Base ####

* You can install WP-Base in your Vagrant Box so that you can look everything up, be able to pull updates, and upgrade other sites you are working on with the fresh updated copy of WP-Base.
* before installing WP-Base you will need to install WPbase-libs which are a bunch of scripts and libraries shared by all sites. To install the libs just do `wpblibs` or if you do not have that shortcut (alias) yet (have not upgraded the box), then you can try `/var/bash/wpblibs.sh`.
* To install WP-Base first make sure your Vagrant box is upgraded, then run `wpbase` command.
* Be sure we already received and authorized your deployment key (see above). Otherwise you won't be able to proceed with the installation.
* So once you run the script WP-Base site will be installed in your vagrant box. the process is very similar to `convert` or `new`.
* Then you will need to add the dopmain `wpb.ewebresults.jay` to your hosts file.
* to update WP-Base run the following commands: `wpb` to jump into the root of your WP-Base installation, `pull` to pull the updates from BitBucket. Then run `dbs` to sync/import the new database. Always choose `local` to import when updating WP-Base. That's it.
* Keep in mind that if you no longer doing active work for us we will disauthorize your deployment key and you won't be able to pull updates. WP-Base is a proprietory piece of software. You are only given access to it because and as long as you work with us.
* To update a site you are working on to newer WP-Base simply replace the `wp-content/themes/ewebresults` folder. You might also need to make some changes to the child theme files to resolve any issues. Also be sure to check for an updated Docs under `WP-Base->Docs->Developers` and any new videos. The videos may help understand new features and how to implement them in the child theme.

#### Next Step [VIDEO](https://youtu.be/Ipc0q8RdXCY) ####

#### [Vagrant Share](https://www.vagrantup.com/docs/share/) ####

Vagrant Share is an extremely useful vagrant feature that allows someone else securely access your vagrant box (by ssh) or a site you host, work on in your vagrant box (by http). Let's say something is not working and you need someone look at it and help you out. This is the kind of situations where this is helpful.

* To enable vagrant share first you need to instyall share plugin `vagrant plugin install vagrant-share`
* Then you want to open a free account with [Vagrant Cloud](https://www.vagrantcloud.com)
* you will also need to install [ngrok](https://ngrok.com/download). If you are on Linux move the downloaded excecutable to `/usr/bin/` by something like `sudo cp ~/Downloads/ngrok /usr/bin/`.
* For the next couple of steps make sure you have your vagrant up, but you are on your host machine - have not vagrant ssh'ed yet or got out of there by `exit`. You need to be in your vagrant directory though on the host machine.
* Do `vagrant login` and login using credentials you obtained from Vagrant Cloud in the previous step.
* If you want to give ssh access do `vagrant share --ssh`. If you want to give http access, do `vagrant share`.
* There will be a name of your share printed on your screen - give it to the party you want to give access to, along with the temporary password you created for the connection. You can always break the connection from your side should you want it by `Ctrl+C`. Otherwise when the purpose of the connection is fulfilled the other part will leave and the connection will be stopped automatically.