#!/bin/bash
# Purpose: WP-Base Development Environment Setup - Bootstrap script
# Author: Jay Gaura < jaygaura@ewebresults.com >
# -------------------------------------------------------------------

# Variables
. /vagrant/vagrant/config

echo -e "\n--- Provisioning virtual machine... ---\n"
echo "=========================================="
echo -e "\n--- Updating Ubuntu ---\n"
sudo apt-get -qq update > /dev/null

# Git
echo -e "\n--- Installing Git & Essentials ---\n"
sudo apt-get -y install git curl build-essential python-software-properties > /dev/null 2>&1
echo -e "\n--- Configuring Git ---\n"
git config --global user.name "$developer_name"
git config --global user.email "$developer_email"
git config --global core.editor nano
git config --global color.ui true

su vagrant -c "git config --global user.name \"${developer_name}\""
su vagrant -c "git config --global user.email $developer_email"
su vagrant -c "git config --global core.editor nano"
su vagrant -c "git config --global color.ui true"

echo -e "\n--- Adding some ppa ---\n"
sudo add-apt-repository ppa:ondrej/php5 > /dev/null 2>&1
echo -e "\n--- Updating packages list ---\n"
sudo apt-get -qq update

echo -e "\n--- Install MySQL specific packages and settings ---\n"
echo "mysql-server mysql-server/root_password password $db_pass" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password $db_pass" | debconf-set-selections
echo "phpmyadmin phpmyadmin/dbconfig-install boolean true" | debconf-set-selections
echo "phpmyadmin phpmyadmin/app-password-confirm password $db_pass" | debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/admin-pass password $db_pass" | debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/app-pass password $db_pass" | debconf-set-selections
echo "phpmyadmin phpmyadmin/reconfigure-webserver multiselect none" | debconf-set-selections
sudo apt-get -y install mysql-server-5.5 phpmyadmin > /dev/null 2>&1

echo -e "\n--- Installing PHP-specific packages ---\n"
sudo apt-get -y install php5 apache2 libapache2-mod-php5 libapache2-mod-auth-mysql php5-curl php5-gd php5-mcrypt php5-mysql php-apc > /dev/null 2>&1

echo -e "\n--- Enabling mod-rewrite ---\n"
sudo a2enmod rewrite > /dev/null 2>&1

echo -e "\n--- We definitly need to see the PHP errors, turning them on ---\n"
sudo sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php5/apache2/php.ini
sudo sed -i "s/display_errors = .*/display_errors = On/" /etc/php5/apache2/php.ini

echo -e "\n--- Turn off disabled pcntl functions so we can use Boris ---\n"
sudo sed -i "s/disable_functions = .*//" /etc/php5/cli/php.ini

echo -e "\n--- Configure Apache to use phpmyadmin ---\n"
echo -e "\n\nListen 81\n" >> /etc/apache2/ports.conf
sudo cat > /etc/apache2/conf-available/phpmyadmin.conf << EOF
<VirtualHost *:81>
    ServerAdmin webmaster@localhost
    DocumentRoot /usr/share/phpmyadmin
    DirectoryIndex index.php
    ErrorLog ${APACHE_LOG_DIR}/phpmyadmin-error.log
    CustomLog ${APACHE_LOG_DIR}/phpmyadmin-access.log combined
</VirtualHost>
EOF
sudo a2enconf phpmyadmin > /dev/null 2>&1
echo -e "\n--- Restarting Apache ---\n"
sudo service apache2 restart > /dev/null 2>&1
echo -e "\n--- Configuring VirtualHost ---\n"
sudo cat > /etc/apache2/sites-available/default << EOF
NameVirtualHost *:80

Include sites-available/eweb.conf
EOF
sudo cat > /etc/apache2/sites-available/eweb.conf << EOF
<VirtualHost *:80>
    ServerAdmin $developer_email
    ServerName e.web
    DocumentRoot /var/www/html
    LogLevel warn
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

    <Directory /var/www/html>
      Options Indexes FollowSymLinks
      AllowOverride All
    </Directory>
</VirtualHost>
EOF
sudo a2dissite 000-default.conf > /dev/null 2>&1
sudo service apache2 restart > /dev/null 2>&1
sudo ln -s /etc/apache2/sites-available/default /etc/apache2/sites-enabled/000-default.conf > /dev/null 2>&1
echo -e "\n--- Restarting Apache ---\n"
sudo service apache2 restart > /dev/null 2>&1
echo -e "\n--- Installing NodeJS and NPM ---\n"
sudo apt-get -y install nodejs > /dev/null 2>&1
sudo ln -s /usr/bin/nodejs /usr/bin/node > /dev/null 2>&1
sudo apt-get -y install npm > /dev/null 2>&1
sudo npm update -y -g npm > /dev/null 2>&1

echo -e "\n--- Installing Gulp ---\n"
sudo npm install -g gulp > /dev/null 2>&1

echo -e "\n--- Installing Composer ---\n"
sudo curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer

echo -e "\n--- Installing WP-Cli ---\n"
sudo curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar > /dev/null 2>&1
sudo chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp
# configuration
mkdir /home/vagrant/.wp-cli
cat > /home/vagrant/.wp-cli/config.yml << EOF
color: true
user: eweb-admin
skip-plugins: 
 - wp-fastest-cache
 - wp-fastest-cache-premium
 - postman-smtp
core config:
  dbuser: root
  dbpass: $db_pass
  extra-php: |
    define('WP_DEBUG', false);
    define('WP_DEBUG_LOG', true);
    define('WP_DEBUG_DISPLAY', false);
    @ini_set('display_errors', 0);
    define('WP_POST_REVISIONS', false);
    define('FS_METHOD', 'direct');
    define('FS_CHMOD_DIR', 0777);
    define('FS_CHMOD_FILE', 0644);
EOF

# Cleanup
sudo apt-get -y autoremove > /dev/null

# shortcuts
echo -e "\n--- Adding Bash Aliases ---\n"
sudo cp /vagrant/vagrant/bash_aliases /home/vagrant/.bash_aliases

# email
echo -e "\n--- Configuring Server Email ---\n"
sudo apt-get -qq update > /dev/null 2>&1
sudo apt-get -y install ssmtp > /dev/null 2>&1
sudo cat > /etc/ssmtp/ssmtp.conf << EOF
#
# Config file for sSMTP sendmail
#
root=$server_email
mailhub=smtp.gmail.com:587
AuthUser=$server_email
AuthPass=$server_email_pass
UseTLS=YES
UseSTARTTLS=YES
hostname=eweb
EOF

#email test

sendmail -oi jaygaura@ewebresults.com << EOF
From: $developer_name <$developer_email>
To: jaygaura@ewebresults.com
Subject: Vagrant setup - email test

name: $developer_name
email: $developer_email
email test

EOF

echo -e "\n--- Changing Promt Color Settings ---\n"
sudo sed -i 's/#force_color_prompt=yes/force_color_prompt=yes/g' /home/vagrant/.bashrc
sudo sed -i 's/#force_color_prompt=yes/force_color_prompt=yes/g' /root/.bashrc
echo -e "\n--- Increasing post_max_size and upload_max_filesize values in php.ini  ---\n"
sudo sed -i 's/post_max_size = 8M/post_max_size = 80M/g' /etc/php5/apache2/php.ini
sudo sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 80M/g' /etc/php5/apache2/php.ini

echo -e "\n--- Finished provisioning ---\n"
