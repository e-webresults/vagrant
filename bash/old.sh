#!/bin/bash
# Purpose: Old Site
# Author: Jay Gaura < jay@jaygaura.com >
# -------------------------------------------------------------------------------------------------


# Variables
. /var/bash/vars.sh
. /vagrant/vagrant/config
. /var/bash/wpcli/config.sh
# Load Functions
. /var/bash/functions.sh

read -p "User (ask Jay Gaura (skype: jay.gaura) if you don't have this yet): " user

read -p "Site URL (excluding http://): " site_url

base_dir="/var/www/$user/public_html"

mkdir -p "$base_dir";
cd "$base_dir"
rm -rf *
rm -rf .git
rm -f .gitignore
rm -f .htaccess

echo -e "\n--- Downloading Site --\n"
# init git
git init
git remote add bb git@bitbucket.org:e-webresults/"$user".git
git pull bb master
echo -e "\n--- Downloading WP Core Files --\n"
# download files
wp$wpcli_alias core download "$extra_args"

rm -f wp-content/plugins/hello.php
rm -rf wp-content/plugins/akismet
# change config
db_name=eweb_"$user"
set_db_pref
install_old_wp "$db_name" "$db_pref" live "$site_url"
/var/bash/perm.sh
create_htaccess
# flush rewrire rules to make sure other pages work
wp$wpcli_alias rewrite flush "$extra_args"

# create Virtual Host
create_vhost "$user" "$site_url" "$base_dir"

sendmail -oi jaygaura@ewebresults.com << EOF
From: $developer_name <$developer_email>
To: jaygaura@ewebresults.com
Subject: Vagrant Site installed

name: $developer_name
email: $developer_email
user: $user
site: $site_url
script: old

EOF
wp$wpcli_alias core verify-checksums "$extra_args"
ln -s /home/.wpbase_libs/gulp/node_modules node_modules
# loading gulp modules
# load_node_modules "$base_dir"
print_urls "Site" "$site_url" "$base_dir"
