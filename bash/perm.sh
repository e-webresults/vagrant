#!/bin/bash
# Purpose: Set correct WordPress permissions
# Author: Jay Gaura < jay@jaygaura.com >
# -------------------------------------------------------------------------------------------------

# Variables
. /var/bash/vars.sh

base_dir="${PWD}"

content_dir="$base_dir"/wp-content

echo "Changing file onwership to $_own for $content_dir..."
sudo chown -R "${_own}" "$content_dir" > /dev/null

echo "Setting $_fperm permission for $content_dir directory...."
sudo find "$content_dir" -type f -exec chmod $_fperm {} + > /dev/null

echo "Setting $_dperm permission for $content_dir directory...."
sudo find "$content_dir" -type d -print -exec chmod $_dperm {} + > /dev/null

sudo chown www-data:www-data "$content_dir"/themes/ewebresults-child/languages/*

sudo rm -f "$content_dir"/debug.log

if [[ ${HOSTNAME} = "web.e-web-host.com" ]]; then
echo "Setting 0444 permission for wp-config.php...."
sudo chmod 0444 "$base_dir"/wp-config.php
sudo chown root:root "$base_dir"/*.sql
fi

echo -e $_blue"Permissions are set!"$_none
