#!/bin/bash
# Purpose: Set 2 Sites for conversion
# Author: Jay Gaura < jay@jaygaura.com >
# -------------------------------------------------------------------------------------------------

# Variables
. /var/bash/vars.sh
. /vagrant/vagrant/config
. /var/bash/wpcli/config.sh
# Load Functions
. /var/bash/functions.sh

echo -e "\n--- This script will install 2 websites in your vagrant box server: the new site that you will be building and the old one so that you can look up the design, have access to database, uploads and the theme images.\n"
echo -e "\n--- In the process you will be prompted to answer 3 questions: the User (you should have received from us) and the 2 local URL's one for the new site and one for the old site. You can use any extension for the urls, but you should not use ones that are publicly available like .com or .net or even .dev. Use something specific like your name, i.e. .jay. Something like example.jay should do. For the old site I would use something like example-old.jay."
echo -e "\n--- The script may take several minutes to run depending on how fast your computer is and how much RAM you allocated to your vagrant box.\n"

read -p "Ready? (Y/n) " ready

read -p "User (ask Jay Gaura (skype: jay.gaura) if you don't have this yet): " user
user_old="$user"_old

read -p "New Site URL (excluding http://): " new_url

read -p "Old Site URL (excluding http://) - must be different from the New Site URL you enetered above: " old_url

new_dir="/var/www/$user/public_html"
old_dir="/var/www/$user/old"

# Old Site

mkdir -p "$old_dir";
cd "$old_dir"
rm -rf *
rm -rf .git
rm -f .gitignore
rm -f .htaccess

echo -e "\n--- Downloading Site --\n"
# init git
git init
git remote add bb git@bitbucket.org:e-webresults/"$user".git
#git pull bb master
git pull bb master
echo -e "\n--- Downloading WP Core Files --\n"
# download files
wp$wpcli_alias core download "$extra_args"

rm -f wp-content/plugins/hello.php
rm -rf wp-content/plugins/akismet
# change config
db_name_old=eweb_"$user_old"
set_db_pref
install_old_wp "$db_name_old" "$db_pref" live "$old_url"
/var/bash/perm.sh
create_htaccess
# flush rewrire rules to make sure other pages work
wp$wpcli_alias rewrite flush "$extra_args"

sendmail -oi jaygaura@ewebresults.com << EOF
From: $developer_name <$developer_email>
To: jaygaura@ewebresults.com
Subject: Vagrant Site installed

name: $developer_name
email: $developer_email
user: $user
site: $old_url
script: old (conversion)

EOF
wp$wpcli_alias core verify-checksums "$extra_args"
print_urls "Old Site" "$old_url" "$old_dir"

# New Site

mkdir -p "$new_dir";
cd "$new_dir"
rm -rf *
rm -rf .git
rm -f .gitignore
rm -f .htaccess

echo -e "\n--- Downloading WP-Base --\n"
# init git
git init
git remote add bb git@bitbucket.org:e-webresults/wp-base.git
git pull bb master
echo -e "\n--- Downloading WP Core Files --\n"
# download files
wp$wpcli_alias core download "$extra_args"

rm -f wp-content/plugins/hello.php
rm -rf wp-content/plugins/akismet
rm -rf wp-content/uploads/*
# copy Uploads from the old site
sudo cp -r "$old_dir"/wp-content/uploads/* "$new_dir"/wp-content/uploads/.
# copy the database from old site
sudo cp -r "$old_dir"/live.sql "$new_dir"/.
# change config
db_name_new=eweb_"$user"
install_old_wp "$db_name_new" "$db_pref" live "$new_url"
wp$wpcli_alias theme activate ewebresults-child "$extra_args"
wp$wpcli_alias user add-cap 1 eweb "$extra_args"
wp$wpcli_alias plugin activate revslider "$extra_args"
/var/bash/perm.sh
create_htaccess

# flush rewrire rules to make sure other pages work
wp$wpcli_alias rewrite flush "$extra_args"

sendmail -oi jaygaura@ewebresults.com << EOF
From: $developer_name <$developer_email>
To: jaygaura@ewebresults.com
Subject: Vagrant Site installed

name: $developer_name
email: $developer_email
user: $user
site: $new_url
script: new

EOF
wp$wpcli_alias core verify-checksums "$extra_args"
# loading gulp modules
load_node_modules "$new_dir"
# re-initiate git
sudo rm -rf .git
git init
# create Virtual Hosts
create_vhost "$user_old" "$old_url" "$old_dir"
create_vhost "$user" "$new_url" "$new_dir"
print_urls "New Site" "$new_url" "$new_dir"
