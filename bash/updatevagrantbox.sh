#!/bin/bash
# Purpose: Update eWebResults Vagrant Box
# Author: Jay Gaura < jay@jaygaura.com >
# ---------------------------------------

# Variables
. /var/bash/vars.sh
. /vagrant/vagrant/config
. /var/bash/wpcli/config.sh
# Load Functions
. /var/bash/functions.sh


cd /home/vagrant/
tmp_dir=$(cat /dev/urandom | tr -dc 'a-z' | fold -w 5 | head -n 1)
mkdir "$tmp_dir"
cd "$tmp_dir"
git clone git@bitbucket.org:e-webresults/vagrant.git .


echo -e "\n--- This script will replace the "$_blue"bash"$_none" "$_blue"vagrant"$_none" folders in your vagrant box.\n"

read -p "Ready (Y/no)? " go_ahead

rm -rf /vagrant/bash
cp -r /home/vagrant/"$tmp_dir"/bash /vagrant/.

cp /vagrant/vagrant/config /home/vagrant/"$tmp_dir"/vagrant/.
rm -rf /vagrant/vagrant
cp -r /home/vagrant/"$tmp_dir"/vagrant /vagrant/.

rm -r /vagrant/README.md
cp /home/vagrant/"$tmp_dir"/README.md /vagrant/.

rm -rf /home/vagrant/"$tmp_dir"

/var/bash/updatealiases.sh

echo -e $_blue"All done!"$_none"\nYou will need to exit out of ssh ("$_blue"exit"$_none") and log back into ssh again ("$_blue"vagrant ssh"$_none")."