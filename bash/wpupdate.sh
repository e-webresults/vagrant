#!/bin/bash
# Purpose: Upgrade WordPress
# Author: Jay Gaura < jay@jaygaura.com >
# -------------------------------------------------------------------------------------------------

base_dir="${PWD}"

# Variables
. /var/bash/wpcli/config.sh

update_is=$(wp core check-update "$extra_args")
available="WordPress is at the latest version"
if [ "${update_is/$available}" = "$available" ] ; then
  read -p "Refresh WP (y/N)? " update
  while [[ -z "$update" ]]; do
    update="n"
  done
  if [ "$new" == "y" ]
  then
    wp core update "$extra_args"
    wp core update-db "$extra_args"
    /var/bash/permall.sh
    wp core verify-checksums "$extra_args"
  fi
else
  echo "$update_is"
fi

wp plugin update --all "$extra_args"
/var/bash/perm.sh

echo -e $_blue"WP Updated!"$_none