#!/bin/bash
# Purpose: Set Gulp Stuff
# Author: Jay Gaura < jay@jaygaura.com >
# -------------------------------------------------------------------------------------------------


base_dir="${PWD}"
cd "$base_dir"
read -p "Symbolic link (vs actual install)? (Y/n) " symb_link
while [[ -z "$symb_link" ]]; do
  symb_link=y
done

if [ "$symb_link" == "y" ]; then
  ln -s /home/.wpbase_libs/gulp/node_modules node_modules
else
  sudo npm install --global gulp
  sudo npm install --save-dev gulp
  sudo npm install --save-dev gulp-uglify
  sudo npm install --save-dev gulp-less
  sudo npm install --save-dev gulp-minify-css
  sudo npm install --save-dev gulp-concat
fi

echo "All done!"
