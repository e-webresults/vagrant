#!/bin/bash
# Purpose: Set correct WordPress permissions
# Author: Jay Gaura < jay@jaygaura.com >
# -------------------------------------------------------------------------------------------------

# Variables
. /var/bash/vars.sh

base_dir="${PWD}"

# permissions
sudo chown -R "$_own" wp-admin
sudo chown -R "$_own" wp-includes
sudo chown "$_own" *.php
sudo chown "$_own" *.html
sudo chown "$_own" *.md
sudo chown "$_own" *.txt
sudo chown "$_own" .htaccess

echo "Setting $_fperm permission for $base_dir directory...."
sudo find "$base_dir" -type f -exec chmod $_fperm {} + > /dev/null

echo "Setting $_dperm permission for $base_dir directory...."
sudo find "$base_dir" -type d -print -exec chmod $_dperm {} + > /dev/null

/var/bash/perm.sh

echo -e "\n"$_blue"All done"$_none"\n"