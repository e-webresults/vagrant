#!/bin/bash
# Purpose: Sync WordPress databases
# Author: Jay Gaura < jay@jaygaura.com >
# -------------------------------------------------------------------------------------------------

# Variables
. /var/bash/vars.sh
. /vagrant/vagrant/config
. /var/bash/wpcli/config.sh
# Load Functions
. /var/bash/functions.sh

base_dir="${PWD}"

git pull bb master

rm -rf "$base_dir"/wp-content/cache > /dev/null

read -p "Do DB Sync Now? (Y/n) " dbsync_now
while [[ -z "$dbsync_now" ]]; do
  dbsync_now=y
done
dbsync_now="${dbsync_now,,}"

if [ "$dbsync_now" == "y" ]; then
    /var/bash/dbsync.sh
  fi

echo -e "\n"$_blue"Pulled successfully!"$_none"\n"
