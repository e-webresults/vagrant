#!/bin/bash
# Purpose: Functions to help eweb vagrant box scripts
# Author: Jay Gaura < jay@jaygaura.com >
# -------------------------------------------------------------------------------------------------

# Functions

create_vhost() {
  if [ "$1" == "" ]; then
    echo "No user provided"
    exit 1
  fi
  if [ "$2" == "" ]; then
    echo "No site_url provided"
    exit 1
  fi
  if [ "$3" == "" ]; then
    echo "No base_dir provided"
    exit 1
  fi
  declare user=$1
  declare site_url=$2
  declare base_dir=$3

  echo -e "\n--- Adding Virtual Host ---\n"

  echo -e "\nInclude sites-available/$user.conf\n"  | sudo tee --append /etc/apache2/sites-available/default > /dev/null

  hosts_line="127.0.0.1 $site_url"
  if ! grep -q -o "$hosts_line" /etc/hosts; then
    echo "$hosts_line"  | sudo tee --append /etc/hosts > /dev/null
  fi
  vhost="$(</vagrant/vagrant/virtual_host.conf)"
  vhost="${vhost//SITE_DOMAIN/$site_url}"
  vhost="${vhost//SITE_PATH/$base_dir}"
  echo "${vhost/ADMIN_EMAIL/$developer_email}" > "$base_dir"/"$user".conf
  sudo mv "$base_dir"/"$user".conf /etc/apache2/sites-available/"$user".conf
  sudo chown root:root /etc/apache2/sites-available/"$user".conf
  sudo chmod 0644 /etc/apache2/sites-available/"$user".conf

  sudo service apache2 restart > /dev/null 2>&1

  echo -e "--- Virtual Host added ---\n"
}

create_htaccess() {
  sudo cat > .htaccess << EOF
# BEGIN WordPress
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteBase /
RewriteRule ^index\.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /index.php [L]
</IfModule>

# END WordPress
EOF
  chown vagrant:www-data .htaccess
}

load_node_modules() {
  if [ "$1" == "" ]; then
    echo "No base_dir provided"
    exit 1
  fi
  declare base_dir=$1
  sudo npm update -y -g npm
  sudo npm install --save-dev gulp-install
  sudo npm install --save-dev gulp
  sudo npm install --save-dev gulp-uglify
  sudo npm install --save-dev gulp-less
  sudo npm install --save-dev gulp-minify-css
  sudo npm install --save-dev gulp-concat
}

remove_default_themes() {
  wp$wpcli_alias theme delete twentythirteen "$extra_args"
  wp$wpcli_alias theme delete twentyfourteen "$extra_args"
  wp$wpcli_alias theme delete twentyfifteen "$extra_args"
  wp$wpcli_alias theme delete twentysixteen "$extra_args"
}

install_old_wp() {
  if [ "$1" == "" ]; then
    echo "No db_name provided"
    exit 1
  fi
  if [ "$2" == "" ]; then
    echo "No db_pref provided"
    exit 1
  fi
  if [ "$3" == "" ]; then
    echo "No dbase provided"
    exit 1
  fi
  if [ "$4" == "" ]; then
    echo "No site_url provided"
    exit 1
  fi
  declare db_name=$1
  declare db_pref=$2
  declare dbase=$3
  declare site_url=$4

  wp$wpcli_alias core config --dbname="$db_name" --dbhost=localhost --dbprefix="$db_pref" "$extra_args"
  wp$wpcli_alias db drop --yes --quiet "$extra_args"
  wp$wpcli_alias db create "$extra_args"
  wp$wpcli_alias db import "$dbase".sql "$extra_args"
  old="$(wp eval 'echo home_url();' "$extra_args" | xargs)"
  wp$wpcli_alias search-replace "$old" http://"$site_url" "$extra_args"
  wp$wpcli_alias user update 1 --display_name="$developer_name" --user_pass="$developer_pass" "$extra_args"
  sudo chown -R "$_own" wp-admin
  sudo chown -R "$_own" wp-includes
  sudo chown "$_own" *.php
  chown "$_own" *.html
  sudo chown "$_own" *.md
  sudo chown "$_own" *.txt
  # remove other themes
  remove_default_themes
}

install_new_wp() {
  if [ "$1" == "" ]; then
    echo "No db_name provided"
    exit 1
  fi
  if [ "$2" == "" ]; then
    echo "No db_pref provided"
    exit 1
  fi
  if [ "$3" == "" ]; then
    echo "No site_url provided"
    exit 1
  fi
  declare db_name=$1
  declare db_pref=$2
  declare site_url=$3

  wp$wpcli_alias core config --dbname="$db_name" --dbhost=localhost --dbprefix="$db_pref" "$extra_args"
  wp$wpcli_alias db drop --yes --quiet "$extra_args"
  wp$wpcli_alias db create "$extra_args"
  wp$wpcli_alias core install --url=http://"$site_url" --title=New --admin_user=eweb-admin --admin_password="$developer_pass" --admin_email="$developer_email" "$extra_args"
  sudo chown -R "$_own" wp-admin
  sudo chown -R "$_own" wp-includes
  sudo chown "$_own" *.php
  chown "$_own" *.html
  sudo chown "$_own" *.md
  sudo chown "$_own" *.txt
  echo -e "\n--- Activate eWebResults Theme ---\n"
  wp$wpcli_alias theme activate ewebresults-child "$extra_args"
  remove_default_themes
  wp$wpcli_alias user add-cap 1 eweb "$extra_args"
  wp$wpcli_alias plugin activate revslider "$extra_args"
}

print_urls() {
  if [ "$1" == "" ]; then
    echo "No site provided"
    exit 1
  fi
  if [ "$2" == "" ]; then
    echo "No site_url provided"
    exit 1
  fi
  if [ "$3" == "" ]; then
    echo "No site_dir provided"
    exit 1
  fi
  declare site=$1
  declare site_url=$2
  declare site_dir=$3
  echo -e "\n--- ${site} installed! ---\n"
  echo -e "\n--- Do not forget to add the new domain $site_url to your hosts file on host computer though!\nlike so\n10.0.0.10  $site_url\n"
  echo -e "\n--- Then access the site at...\nAdmin: http://$site_url/wp-admin\nSite: http://$site_url\non your host machine\n"
  echo -e "\n--- Your files are in $site_dir\n"
}

is_wp_installed_here() {
  declare site_dir=$1
  declare out=0
  if [[ "$site_dir" =~ /var/www/([-_0-9a-z]+)/(public_html|old) ]]; then
    user=${BASH_REMATCH[1]}
    cd "$site_dir"
    declare wp_version=$(wp core --quiet version)
    if [[ "$wp_version" =~ [.0-9]+ ]]; then
      out=1
    fi
  fi
return "$out"
}

set_db_pref() {
  db_pref=$(egrep -o 'Table structure for table `([a-z0-9]+_)posts`' local.sql | grep -o -P "(\ *[a-z]+_)")
}
