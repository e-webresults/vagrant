#!/bin/bash
# Purpose: Create Virtual Host (without anything else)
# Author: Jay Gaura < jay@jaygaura.com >
# -------------------------------------------------------------------------------------------------

# Variables
. /var/bash/vars.sh
. /vagrant/vagrant/config
. /var/bash/wpcli/config.sh
# Load Functions
. /var/bash/functions.sh

read -p "User (ask Jay Gaura (skype: jay.gaura) if you don't have this yet): " user

read -p "Site URL (excluding http://): " site_url

base_dir="/var/www/$user/public_html"

cd "$base_dir"

if [ ! -f ".htaccess" ]; then
  create_htaccess
fi

# create Virtual Host
create_vhost "$user" "$site_url" "$base_dir"

print_urls "Site" "$site_url" "$base_dir"

