#!/bin/bash
# Purpose: Export WordPress database
# Author: Jay Gaura < jay@jaygaura.com >
# -------------------------------------------------------------------------------------------------

tables=$(wp$wpcli_alias db tables --all-tables-with-prefix --format=csv --quiet)
read -p "Keep comments? You should only say 'yes' to this if this is a new site you just developed. Otherwise hit ENTER to accept default - NO. (yes/NO) " keepc
while [[ -z "$keepc" ]]; do
  keepc=no
done
if [ "$keepc" == "no" ]; then
  # remove comments
  tables=$(echo "$tables" | sed -r 's/,?[a-z]{2}_comment(s|meta)//g')
fi
wp$wpcli_alias db export --tables="$tables" local.sql
