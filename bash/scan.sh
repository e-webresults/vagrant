#!/bin/bash
# Purpose: Scan WP install with ClamAV
# Author: Jay Gaura < jay@jaygaura.com >
# -----------------------------------------------------------------------------$

base_dir="${PWD}"

_red="\033[1;31;40m"
_blue="\033[1;36;40m"
_none="\033[0m"

sudo clamscan -r "$base_dir" | grep FOUND >> "$base_dir"/scan_report.txt

cat "$base_dir"/scan_report.txt

echo -e $_blue"Scan finished. See report above or/and in scan_report.txt"$_none

