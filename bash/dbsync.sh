#!/bin/bash
# Purpose: Sync WordPress databases
# Author: Jay Gaura < jay@jaygaura.com >
# -------------------------------------------------------------------------------------------------

# Variables
. /var/bash/vars.sh
. /vagrant/vagrant/config
. /var/bash/wpcli/config.sh
# Load Functions
. /var/bash/functions.sh

base_dir="${PWD}"

# make sure the call comes from the right place
is_wp_installed_here "$base_dir"
if [ "$?" == 0 ]; then
  echo -e "\n"$warning"Calling from a "$_red"wrong place"$_none"!\n"$info"Should be called from site root (i.e. "$_blue"/var/www/mysite/public_html"$_none")\n"
  exit 1
fi
echo "Databases available: "
ls *.sql
read -p "Which DB do you want to import? (LIVE/local) " db
while [[ -z "$db" ]]; do
  db=live
done

wp"$wpcli_alias"db export tmp.sql > /dev/null

new="$(wp$wpcli_alias eval --quiet 'echo home_url();' "$extra_args")"
new=${new%/}
wp$wpcli_alias db import "$db".sql "$extra_args"
old="$(wp$wpcli_alias eval --quiet 'echo home_url();' "$extra_args"cat)"
old=${old%/}
echo "Replacing $old with $new"
wp$wpcli_alias search-replace "$old" "$new" "$extra_args"

tables=$(wp$wpcli_alias db tables *_posts --quiet)
dbprefs=$(echo "$tables" | wc -l)
if [[ "$dbprefs" > 1 ]]; then
  echo -e "\n"$_red"Attention:"$_none" You have more than 1 DB Prefs--\n"
  read -p 'Do you want to restore your DB, consolidate the prefs (in wp-config.php), and then run DBS again? (Y/n) ' restore
  while [[ -z "$restore" ]]; do
    restore=yes
  done
  if [[ "$restore" = "yes" ]]; then
    wp$wpcli_alias db import tmp.sql --quiet
  fi
fi
rm -f tmp.sql > /dev/null

echo -e "\n"$_blue"Database synced successfully!"$_none"\n"
