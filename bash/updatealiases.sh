#!/bin/bash
# Purpose: Update Bash Aliases
# Author: Jay Gaura < jay@jaygaura.com >
# -------------------------------------------------------------------------------------------------

rm -f ~/.bash_aliases
cp /vagrant/vagrant/bash_aliases ~/.bash_aliases
echo -e "\n--- Aliases successfully updated. You need to exit and then vagrant ssh back again for the new aliases to take effect. ---\n"