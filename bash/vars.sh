#!/bin/bash

# permissions
_fperm="0644"
_dperm="0777"
_own="vagrant:www-data"
# colors
_red="\033[1;31;40m"
_blue="\033[1;36;40m"
_none="\033[0m"
warning="$_red ! $_none"
info="$_blue i $_none"
