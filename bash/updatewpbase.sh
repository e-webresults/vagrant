#!/bin/bash
# Purpose: Update WP-Base
# Author: Jay Gaura < jay@jaygaura.com >
# -------------------------------------------------------------------------------------------------

base_dir="${PWD}"
wpbase_root="/var/www/wpbase/public_html"

# Variables
. /var/bash/vars.sh
. /vagrant/vagrant/config
. /var/bash/wpcli/config.sh
# Load Functions
. /var/bash/functions.sh

read -p "Which version on WP-Base to use 1.3 or 1.4 (master)? " version

# make sure the call comes from the right place
is_wp_installed_here "$base_dir"
if [ "$?" == 1 ]; then
  # make sure wpbase is installed
  is_wp_installed_here "$wpbase_root"
  if [ "$?" == 1 ]; then
    cd "$wpbase_root"
    git checkout master
    git pull bb master
    if [ "$version" == 1.3 ]; then
      git checkout 1.3
      git pull bb 1.3
    fi
  else
    echo -e "\n"$warning"WP-Base is not installed in "$_blue"/var/www/wpbase/public_html"$_none")\n"
    echo -e "\n"$info"Run "$_blue"wpbase"$_none" to install it\n"
    exit 1
  fi
else
  echo -e "\n"$warning"Calling from a "$_red"wrong place"$_none"!\n"$info"Should be called from site root (i.e. "$_blue"/var/www/mysite/public_html"$_none")\n"
  exit 1
fi

echo -e "\nThis script will replace the "$_blue"ewebresults"$_none" folder and the "$_blue"usual.less"$_none" file.\n Everything else will need to be done manually.\n"
echo -e "\nSee "$_blue"WP-Base->Docs->Release Notes and Upgrade Instructions"$_none" for instructions on what needs to be done. The upgrade process will likely be different for each version update.\nAlso even though the script has made an effort to update WP-Base it is your responsibility to insure that it is updated in your vagrant box.\n"

read -p "Ready (Y/no)? " go_ahead

# ewebresults
rm -rf "$base_dir"/wp-content/themes/ewebresults
cp -r "$wpbase_root"/wp-content/themes/ewebresults "$base_dir"/wp-content/themes/.

# mixins
rm -rf "$base_dir"/.assets/bootstrap_custom/mixins
cp -r "$wpbase_root"/.assets/bootstrap_custom/mixins "$base_dir"/.assets/bootstrap_custom/.

# fancybox
rm -rf "$base_dir"/.assets/fancybox
cp -r "$wpbase_root"/.assets/fancybox "$base_dir"/.assets/.

# owl
rm -rf "$base_dir"/.assets/owl.carousel
cp -r "$wpbase_root"/.assets/owl.carousel "$base_dir"/.assets/.

# usual.less
rm -rf "$base_dir"/.assets/bootstrap_custom/usual.less
cp "$wpbase_root"/.assets/bootstrap_custom/usual.less "$base_dir"/.assets/bootstrap_custom/.

# update the version
current=$(wp$wpcli_alias eval-file /var/bash/wpcli/childversion.php)
new=$(wp$wpcli_alias eval-file /var/bash/wpcli/parentversion.php)
sed -i "s/${current}/${new}/" "$base_dir"/wp-content/themes/ewebresults-child/style.css

echo -e $_blue"Replacement"$_none" is done. Now please follow the instructions in "$_blue"WP-Base->Docs->Release Notes and Upgrade Instructions"$_none" to complete the update."
