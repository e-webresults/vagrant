#!/bin/bash
# Purpose: Clean Up script
# Author: Jay Gaura < jay@jaygaura.com >
# -------------------------------------------------------------------------------------------------


echo -e "\n--- This script will attempt to clean up after an unsuccessful site installation. ---\n"

read -p "User (ask Jay Gaura (skype: jay.gaura) if you don't have this yet): " user

# remove files
sudo rm -rf /var/www/$user
# remove virtual host
sudo sed -i "/$user.conf$/d" /etc/apache2/sites-available/default
# remove blank lines
sudo -i sed '/^ *$/d' /etc/apache2/sites-available/default
# remove the hosts file itself
sudo rm -f /etc/apache2/sites-available/"$user".conf
# remove host line
sudo -i sed "/$user/d" /etc/hosts
# restart apache
sudo service apache2 restart > /dev/null 2>&1

echo -e "\n--- Clean Up is done. You can run 'old' or 'new' script again now. ---\n"