#!/bin/bash
# Purpose: Install WPBase_Libs
# Author: Jay Gaura < jay@jaygaura.com >
# -------------------------------------------------------------------------------------------------

# Variables
. /var/bash/vars.sh
. /vagrant/vagrant/config
. /var/bash/wpcli/config.sh
# Load Functions
. /var/bash/functions.sh

cd /home
git clone git@bitbucket.org:e-webresults/wpbase_libs.git
mv wpbase_libs .wpbase_libs
cd /home/.wpbase_libs
composer self-update
composer update
echo -e "\n--- Congrats! WPbase_libs are installed --\n"
