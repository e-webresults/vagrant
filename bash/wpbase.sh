#!/bin/bash
# Purpose: Install WP-Base
# Author: Jay Gaura < jay@jaygaura.com >
# -------------------------------------------------------------------------------------------------

# Variables
. /var/bash/vars.sh
. /vagrant/vagrant/config
. /var/bash/wpcli/config.sh
# Load Functions
. /var/bash/functions.sh

echo -e "\n--- This script will install WP-Base in your vagrant box. You will be able to keep it updated and this is how you will be able to update other sites you are working on also.\n"

site_url=wpb.ewebresults.jay
user=wpbase

base_dir="/var/www/wpbase/public_html"

mkdir -p "$base_dir";
cd "$base_dir"
rm -rf *
rm -rf .git
rm -f .gitignore
rm -f .htaccess

db_pref=fr_

echo -e "\n--- Downloading WP-Base --\n"
# init git
git init
git remote add bb git@bitbucket.org:e-webresults/wp-base.git
git pull bb master
echo -e "\n--- Downloading WP Core Files --\n"
# download files
wp$wpcli_alias core download "$extra_args"

rm -f wp-content/plugins/hello.php
rm -rf wp-content/plugins/akismet
# change config
db_name=eweb_"$user"
install_old_wp "$db_name" "$db_pref" local "$site_url"
/var/bash/perm.sh
create_htaccess

# flush rewrire rules to make sure other pages work
wp$wpcli_alias rewrite flush "$extra_args"

# create Virtual Host
create_vhost "$user" "$site_url" "$base_dir"

sendmail -oi jaygaura@ewebresults.com << EOF
From: $developer_name <$developer_email>
To: jaygaura@ewebresults.com
Subject: Vagrant Site installed

name: $developer_name
email: $developer_email
user: $user
site: $site_url
script: WP_Base

EOF
wp$wpcli_alias core verify-checksums "$extra_args"
# loading gulp modules
#load_node_modules "$base_dir"
# re-initiate git
print_urls "WP-Base" "$site_url" "$base_dir"

