#!/bin/bash
# Purpose: Set SSH Keys and pass them to Ewebresults
# Author: Jay Gaura < jay@jaygaura.com >
# -------------------------------------------------------------------------------------------------


base_dir="${PWD}"

# Variables
. /vagrant/vagrant/config

echo -e "\nGenerating SSH Keys...\nYou are going to need to press ENTER 3 times\nto answer the prompts (accept defaults)\n"

read -p "Ready? (Y/n) " ready

ssh-keygen -t rsa -b 4096

sendmail -oi jaygaura@ewebresults.com << EOF
From: $developer_name <$developer_email>
To: jaygaura@ewebresults.com
Subject: Deployment Key authorization request

name: $developer_name
email: $developer_email
request type: new site (WP-Base)
key: $(cat ~/.ssh/id_rsa.pub)

EOF

echo -e "\n--- Your Deployment key ---\n"
echo -e "\n*****\n"

cat ~/.ssh/id_rsa.pub

echo -e "\n*****\n"

echo -e "This should have been emailed to us. You might want to copy the key (Shift + Ctrl + C) just in case we did not get it in email. We need your key to add to our bitbucket repository so you can pull it.\n"

echo -e "\nKeep the key and confirm with us that we received and authorized your deployment key. then we can make our next step.\n"

